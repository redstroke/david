<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$ability_labels = ['god' => 'Бессмертие', 'fly' => 'Полет', 'idclip' => 'Прохождение сквозь стены', 'fireball' => 'Файрболлы'];
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['passw'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['passw']));
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();

      $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['legs'] = !empty($_COOKIE['legs_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['biography'] = !empty($_COOKIE['biohraphy_error']);
  $errors['check1'] = !empty($_COOKIE['check1_error']);

  
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
    if($_COOKIE['fio_error'] == '1') 
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя.</div>';
	else $messages[]='<div class="error"> Недопустимые символы в имени.</div>';
}
  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
    if($_COOKIE['email_error'] == '1') 
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните email.</div>';

}
   
if ($errors['year']) {
    setcookie('year_error', '', 100000);
     if($_COOKIE['fio_error'] == '1') 
    $messages[] = '<div class="error">Заполните год.</div>';
	else $messages[]='<div class="error"> Недопустимые символы в году.</div>';
  }


if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $messages[] = '<div class="error"> Не выбран пол </div>';
  }


if ($errors['abilities']) {
    setcookie('abilities_error', '', 100000);
    $messages[] = '<div class="error"> Не выбрана супер </div>';
  }


if ($errors['legs']) {
    setcookie('legs_error', '', 100000);
    $messages[] = '<div class="error"> Не выбраны конечности </div>';
}

if ($errors['biography']) {
    setcookie('biography_error', '', 100000);
    $messages[] = '<div class="error"> Пустая биография </div>';
}

if ($errors['check1']) {
    setcookie('check1_error', '', 100000);
    $messages[] = '<div class="error"> Необходимо ваше согласие! </div>';
}

  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
 if (isset($_COOKIE['fio_value']))
  $values['fio'] = empty($_COOKIE['fio_value']) || !preg_match('/^[а-яА-Я ]+$/u', $_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  else $values['fio'] ='';

  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
$values['year'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
$values['legs'] = empty($_COOKIE['legs_value']) ? '' : strip_tags($_COOKIE['legs_value']);
$values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : strip_tags($_COOKIE['abilities_value']);
$values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
$values['check1'] = empty($_COOKIE['check1_value']) ? '' : strip_tags($_COOKIE['check1_value']);

  // TODO: аналогично все поля.

  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // Загрузить данные пользователя из БД.
    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
   
  $ability_insert = [];
$user = 'u15851';
$pass = '6322046';
$db = new PDO('mysql:host=localhost;dbname=u15851', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    // Подготовленный запрос. Не именованные метки.
    try {
      $stmt = $db->prepare("SELECT * FROM application2 WHERE id = ?");
      $stmt->execute(array($_SESSION['uid']));
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    } 
  
    $user_data = $stmt->fetchAll();
    $values['fio'] = !empty($user_data[0]['name']) ? $user_data[0]['name'] : '';
    $values['email'] = !empty($user_data[0]['email']) ? $user_data[0]['email'] : '';
    $values['year'] = !empty($user_data[0]['year']) ? $user_data[0]['year'] : '';
    $values['sex'] = !empty($user_data[0]['sex']) ? $user_data[0]['sex'] : '';
    $values['legs'] = !empty($user_data[0]['legs']) ? $user_data[0]['legs'] : '';
    $ability_insert['god'] = !empty($user_data[0]['ability_god']) ? $user_data[0]['ability_god'] : '';
    $ability_insert['fly'] = !empty($user_data[0]['ability_fly']) ? $user_data[0]['ability_fly'] : '';
    $ability_insert['idclip'] = !empty($user_data[0]['ability_idclip']) ? $user_data[0]['ability_idclip'] : '';
    $ability_insert['fireball'] = !empty($user_data[0]['ability_fireball']) ? $user_data[0]['ability_fireball'] : '';
    $values['biography'] = !empty($user_data[0]['biography']) ? $user_data[0]['biography'] : '';
    $values['check1'] = !empty($user_data[0]['check1']) ? $user_data[0]['check1'] : '';
  }  

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
 if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) 
    // Выдаем куку на день с флажком об ошибке в поле fio.
   { setcookie('fio_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;}
    else {
    // Сохраняем ранее введенное в форму значение на месяц.
    
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);}
  
  
  if (empty($_POST['email']) ) {
		setcookie('email_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['year'])) {
		setcookie('year_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else { 
	    $year = $_POST['year'];
        if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) <= 2020)) 
        setcookie('year_error', '2', time() + 24 * 60 * 60);
        	
    	else
		setcookie('year_value', $_POST['year'], time() + 365 * 24 * 60 * 60);
}

	if ( empty($_POST['sex'])) {
		setcookie('sex_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
	}
	

	if (empty($_POST['legs'])) {
		setcookie('legs_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('legs_value', $_POST['legs'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['abilities'])) {
		setcookie('abilities_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else 
	{
		$abilities =serialize($_POST['abilities']);
		setcookie('abilities_value', $abilities, time() + 365 * 24 * 60 * 60);
	}

$ability_data = array_keys($ability_labels);
$ability_insert = [];
$abilities = $_POST['abilities'];
  foreach ($ability_data as $ability)
  
{ $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;}

if (empty($_POST['biography'])) {
		setcookie('biography_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('biography_value', $_POST['biography'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['check1'])) {
		setcookie('check1_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		$check1 = $_POST['check1'];
		setcookie('check1_value', $check1, time() + 365 * 24 * 60 * 60);
	}


// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
     setcookie('email_error', '', 100000);
        setcookie('year_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('legs_error', '', 100000);
        setcookie('abilities_error', '', 100000);
        setcookie('biography_error', '', 100000);
        setcookie('check1_error', '', 100000);

    // TODO: тут необходимо удалить остальные Cookies.
  }
  /*
$user = 'u15851';
$pass = '6322046';
$db = new PDO('mysql:host=localhost;dbname=u15851', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application2 SET name = ?, email = ?, year = ?, sex = ?, legs = ?, ability_god = ?, ability_fly = ?, ability_idclip = ?, ability_fireball = ?, biography = ?, check1 = ?");
  $stmt->execute([$_POST['fio'], $_POST['email'], intval($year), $_POST['sex'], $_POST['legs'], $ability_insert['god'], $ability_insert['fly'], $ability_insert['idclip'], $ability_insert['fireball'], $_POST['biography'], $_POST['check1']]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

*/

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // Перезаписать данные в БД новыми данными,

$user = 'u15851';
$pass = '6322046';
$db = new PDO('mysql:host=localhost;dbname=u15851', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


    // Подготовленный запрос. Не именованные метки.
    try {
      $stmt = $db->prepare("UPDATE application2 SET name = ?, email = ?, year = ?, sex = ?, legs = ?, ability_god = ?, ability_fly = ?, ability_idclip = ?, ability_fireball = ?, biography = ?, check1 = ? WHERE id = ?");
      $stmt->execute([$_POST['fio'], $_POST['email'], intval($year), $_POST['sex'], $_POST['legs'], $ability_insert['god'], $ability_insert['fly'], $ability_insert['idclip'], $ability_insert['fireball'], $_POST['biography'], $_POST['check1'], $_SESSION['uid']]);
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    } 
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $login =  $_POST['email'];
    $passw = substr(uniqid(),0,8);
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('passw', $passw);
}
    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    // ...
    $user = 'u15851';
$pass = '6322046';
$db = new PDO('mysql:host=localhost;dbname=u15851', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


    // Подготовленный запрос. Не именованные метки.
   /* try {
      $stmt = $db->prepare("UPDATE application2 SET login = ?, pass = ? WHERE id = ?");
      $stmt->execute(array($login, md5($passw), $_SESSION['uid']));
    }*/
    try {
  $stmt = $db->prepare("INSERT INTO application2 SET name = ?, email = ?, year = ?, sex = ?, legs = ?, ability_god = ?, ability_fly = ?, ability_idclip = ?, ability_fireball = ?, biography = ?, check1 = ?, login = ?, pass = ?");
  $stmt->execute([$_POST['fio'], $_POST['email'], intval($year), $_POST['sex'], $_POST['legs'], $ability_insert['god'], $ability_insert['fly'], $ability_insert['idclip'], $ability_insert['fireball'], $_POST['biography'], $_POST['check1'], $login,  md5($passw)]);
}
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    } 
  

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: ./');
}
